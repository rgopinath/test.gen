#!/bin/env ruby

require 'rubygems'
require 'nokogiri'

begin

doc = Nokogiri::XML(File.open(ARGV[0])) do |c|
  c.default_xml.noblanks
end

def get_dependencies(doc)
  deps = []
  doc.css('//dependencies/dependency').each do |node|
    grpId = node.at_css("groupId").text
    artId = node.at_css("artifactId").text
    verId = node.at_css("version").text
    deps << [grpId, artId, verId].join(':')
  end
  return deps
end

reqdeps = [
  ['net.sourceforge.javydreamercsw', 'randoop', '1.3.2', nil, nil],
  ['jtestcase.a', 'a', '0.0', '/scratch/research/test.gen/lib/jtestcase.jar', 'system'],
  ['jcrasher', 'a', '0.0', '/scratch/research/test.gen/lib/jcrasher.jar', 'system'],
  ['xmlutil', 'a', '0.0', '/scratch/research/test.gen/lib/xmlutil.jar', 'system'],
  ['junit', 'junit', '4.10', nil, 'test'],
]

doc.css('//dependency').each do |node|
  case node.content
  when /junit/i
    node.remove
  when /jtestcase/i
    node.remove
  when /jcrasher/i
    node.remove
  when /xmlutil/i
    node.remove
  end
end



deps =  doc.at_css('//dependencies')
if deps.nil?
  proj = doc.at_css('//project')
  deps = Nokogiri::XML::Node.new "dependencies", doc
  proj.add_child(deps)
end

reqdeps.each do |dd|
  gt = dd[0]
  at = dd[1]
  v = dd[2]
  p = dd[3]
  s = dd[4]

  d = Nokogiri::XML::Node.new "dependency", doc
  gid = Nokogiri::XML::Node.new "groupId", doc
  gid.content = gt
  d.add_child(gid)

  artid = Nokogiri::XML::Node.new "artifactId", doc
  artid.content = at
  d.add_child(artid)

  ver = Nokogiri::XML::Node.new "version", doc
  ver.content = v
  d.add_child(ver)

  if !p.nil?
    path = Nokogiri::XML::Node.new "systemPath", doc
    path.content = p
    d.add_child(path)
  end

  if !s.nil?
    scope = Nokogiri::XML::Node.new "scope", doc
    scope.content = s
    d.add_child(scope)
  end

  deps.add_child(d)
end

build = doc.at_css('//project/build')
if build.nil?
  proj = doc.at_css('//project')
  build = Nokogiri::XML::Node.new "build", doc
  proj.add_child(build)
end

plugins =  doc.at_css('//build/plugins')
if plugins.nil?
  plugins = Nokogiri::XML::Node.new "plugins", doc
  build.add_child(plugins)
end

rep =  doc.at_css('//reporting')
if rep.nil?
  proj = doc.at_css('//project')
  rep = Nokogiri::XML::Node.new "reporting", doc
  proj.add_child(rep)
end

repplugins =  doc.at_css('//reporting/plugins')
if repplugins.nil?
  proj = doc.at_css('//project/reporting')
  repplugins = Nokogiri::XML::Node.new "plugins", doc
  proj.add_child(repplugins)
end

doc.css('//build/plugins/plugin').each do |node|
  case node.content
  when /org.pitestpitest/
    node.remove
  when /org.codehaus.mojo/
    node.remove
  when /maven-surefire-plugin/
    node.remove
  end
end

def add_plugin(doc, gid_text, artid_text, ver_text, &block)
  p = Nokogiri::XML::Node.new "plugin", doc
  gid = Nokogiri::XML::Node.new "groupId", doc
  gid.content = gid_text
  p.add_child(gid)

  artid = Nokogiri::XML::Node.new "artifactId", doc
  artid.content = artid_text
  p.add_child(artid)

  if ver_text
    ver = Nokogiri::XML::Node.new "version", doc
    ver.content = ver_text
    p.add_child(ver)
  end
  yield p
  return p
end

add_plugin(doc, 'org.pitest', 'pitest-maven', '0.25') do |p|
  conf = Nokogiri::XML::Node.new "configuration", doc
  rep = Nokogiri::XML::Node.new "reportsDirectory", doc
  xml = Nokogiri::XML::Node.new "outputFormats", doc
  val = Nokogiri::XML::Node.new "value", doc
  val.content = 'XML'
  xml.add_child(val)
  rep.content = 'pit.reports'
  conf.add_child(rep)
  conf.add_child(xml)
  p.add_child(conf)
  plugins.add_child(p)
end

if true
  if false
  add_plugin(doc, 'org.codehaus.mojo', 'emma-maven-plugin', '1.0-alpha-3') do |p|
    inh = Nokogiri::XML::Node.new "inherited", doc
    inh.content = 'true'
    p.add_child(inh)

    exes = Nokogiri::XML::Node.new "executions", doc
    exe = Nokogiri::XML::Node.new "execution", doc
    phase = Nokogiri::XML::Node.new "phase", doc
    phase.content = 'process-classes'
    exe.add_child(phase)

    goals = Nokogiri::XML::Node.new "goals", doc
    goal = Nokogiri::XML::Node.new "goal", doc
    goal.content = 'instrument'
    goals.add_child(goal)

    exe.add_child(goals)
    exes.add_child(exe)

    p.add_child(exes)
    plugins.add_child(p)
  end


  add_plugin(doc, 'org.apache.maven.plugins', 'maven-surefire-plugin', nil) do |p|
    inh = Nokogiri::XML::Node.new "inherited", doc
    inh.content = 'true'
    p.add_child(inh)

    conf = Nokogiri::XML::Node.new "configuration", doc
    fm = Nokogiri::XML::Node.new "forkMode", doc
    fm.content = 'once'
    conf.add_child(fm)

    rf = Nokogiri::XML::Node.new "reportFormat", doc
    rf.content = 'xml'
    conf.add_child(rf)

    cd = Nokogiri::XML::Node.new "classesDirectory", doc
    cd.content = '${project.build.directory}/generated-classes/emma/classes'
    conf.add_child(cd)

    p.add_child(conf)
    plugins.add_child(p)
  end
  end
  add_plugin(doc, 'org.codehaus.mojo', 'emma-maven-plugin', '1.0-alpha-3') do |p|
    inh = Nokogiri::XML::Node.new "inherited", doc
    inh.content = 'true'
    p.add_child(inh)
    repplugins.add_child(p)
  end

  add_plugin(doc, 'org.codehaus.mojo', 'surefire-report-maven-plugin', nil) do |p|
    inh = Nokogiri::XML::Node.new "inherited", doc
    inh.content = 'true'
    p.add_child(inh)
    repplugins.add_child(p)
  end
end

puts doc

rescue Exception => e
  puts e.message
  puts e.backtrace
end
