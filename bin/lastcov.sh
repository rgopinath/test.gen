#!/bin/sh
type=$1
test=$2
curdir=$(basename $(pwd))
case $test in
  cobertura)
  per=$(cat ./target/site/cobertura/coverage.xml | awk '/^<coverage/{print $3}' | sed -e 's#.*=##g' -e 's#["]##g');
  score=$( echo 5k $per 100 '*' f | dc )
  echo $type $curdir Branch Coverage : $score
  ;;
  emma)
  score=$(cat emma.*.txt | awk '/all classes/{print $7}' | sed -e 's#[()!%]##g' )
  oth=$(cat emma.*.txt | awk '/all classes/{print $8}' | sed -e 's#[()!%]##g' )
  echo $type $curdir Line Coverage $oth : $score
  ;;
  pit)
  dir=$(ls pit.reports/ | sort -ru | head -1)
  detected=$(cat pit.reports/$dir/mutations.xml | grep "detected='true'" | wc -l)
  all=$(cat pit.reports/$dir/mutations.xml | grep "detected=" | wc -l)
  score=$( echo 5k $detected $all / 100 '*' f | dc )
  echo $type $curdir Mutation Score ${detected}/${all} : $score
  ;;
  codecover)
  elinks --dump target/site/codecover/report.html
  ;;
  mockit)
  elinks --dump coverage-report/index.html
  ;;
  *)
  echo emma cobertura pit codecover mockit
esac
