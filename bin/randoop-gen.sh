#!/bin/bash -x
mkdir -p src/test/java
type=randoop
root=$(/bin/readlink -f $PWD/../../)
case "$1" in
  -x) tar -xvpf ${root}/db/files/${type}.${project}.tar;;
  -c) pkg=`${root}/bin/getgroupid.rb pom.xml`
      path=`echo $pkg | sed -e 's#\.#/#g'`
      mkdir -p src/test/java/${path}/randoop && touch src/test/java/${path}/randoop/RandoopTest.java
      cat <<EOF > .randoop.createtests.sh
      java -cp $(<.classpath.mvn) randoop.main.Main gentests \
        --classlist=./.classes --silently-ignore-bad-class-names=true \
        --junit-package-name=${pkg}.randoop \
        --junit-output-dir=src/test/java --capture-output=true --output-tests=pass \
        --timelimit=100 --timeout=1000
EOF
      chmod +x .randoop.createtests.sh
      bash -x ./.randoop.createtests.sh
      tar -cf ${root}/db/files/${type}.${project}.tar src/test/java
      ;;
  *) echo "$0 -(x,c) env:project,timeout";;
esac
