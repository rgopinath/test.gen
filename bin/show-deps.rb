#!/bin/env ruby
projects = {}
File.readlines('logs/log.txt').grep(/Could not resolve dependencies for project/).each do |line|
  case line
  when /^([^:]+):.*The following artifacts could not be resolved/
    a=$1
    x = line.gsub(/.*The following artifacts could not be resolved:/,'').gsub(/: The repository system is offline but the artifact.*$/,'')
    y = x.split(',').map(&:strip)
    projects[a] ||= []
    projects[a] += y
    y.each do |i|
      #puts " #{i}\t#{a}"
      #projects[i] << a
    end
  when /^([^:]+):.*The repository system is offline but the artifact ([^ ]+) .*$/
    a=$1
    i=$2
    #puts " #{i}\t#{a}"
    projects[a] ||= []
    projects[a] << i
  else
    p line
  end
end
deps = {}
projects.keys.each do |p|
  projects[p].each do |d|
    deps[d] ||= []
    deps[d] << p
  end
end

case ARGV[0]
when /-a/
  deps.keys.each do |d|
    puts "#{d}\t #{deps[d].join(', ')}"
  end
when /-n/
  deps.keys.each do |d|
    puts "#{deps[d].length} #{d}\t #{deps[d].join(', ')}"
  end
when /-z/
  projects.keys.each do |k|
    puts "#{k}\t#{projects[k]}"
  end
else
  puts "#{$0} -(a,z,n)"
end

#| cut -d: -f 6,7,8,9,10 | while read a;
#do
#  case $a in
#    "Could not find artifact"*) echo $a | cut -d\  -f 5;;
#    "The following artifacts could not be resolved:"*) echo $a | cut -d\  -f 8;;
#    "Failure to find"*) echo $a | cut -d\  -f 4;;
##    "Failed to collect dependencies for "*) echo $a | cut -d\  -f 6;;
#    *) echo $a;;
#  esac
#done

