#!/usr/bin/env ruby
#mvn install:install-file  -Dfile=path-to-your-artifact-jar -DgroupId=your.groupId -DartifactId=your-artifactId -Dversion=version -Dpackaging=jar
#mvn install:install-file -Dfile=$1 -DpomFile=$2

s=ARGV[1]
values = s.split(':')

puts %[mvn install:install-file  -Dfile=#{ARGV[0]} -DgroupId=#{values[0]} -DartifactId=#{values[1]} -Dversion=#{values[3]} -Dpackaging=#{values[2]}]
