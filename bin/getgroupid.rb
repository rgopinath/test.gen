#!/bin/env ruby

require 'rubygems'
require 'nokogiri'

doc = Nokogiri::XML(File.open(ARGV[0])) do |c|
  c.default_xml.noblanks
end

puts doc.at_css('/project/groupId').text
