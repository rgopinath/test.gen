#!/usr/bin/ruby

#Name, (Original Emma,Original Mutation) (Randoop Emma, Randoop Mutation)

def lastf(p, ttype, dir)
  lst = Dir.glob("#{dir}/#{ttype}.#{p}.*").sort_by{|f| File.ctime(f)}
  lst[0]
end

def read_pit(p, fname)
  return -1 if fname.nil?
  begin
    s = File.read(fname).chomp.split(/:/)
    s[-1].chomp
  rescue
    -1
  end
end
def read_emma(p, fname)
  return -1 if fname.nil?
  begin
    s = File.readlines(fname).grep(/all classes/)[0].split(/\s+/)
    s[6]
  rescue
    -1
  end
end

loc = {}
tloc = {}
def show_cloc(p)
  #x = %x[./bin/cloc.pl --quiet --csv --match-f='java$' projects/#{p}].split(/\n/)
  #x[2].split(/,/)[-1]
  
end

File.readlines('db/all.java_loc.list').each do |x|
  x.chomp!
  y = x.split(/ +/)
  loc[y[1]] = y[0].to_i
end


File.readlines('db/all.test_loc.list').each do |x|
  x.chomp!
  y = x.split(/ +/)
  tloc[y[1]] = y[0].to_i
end



projects = File.open('etc/projects.txt').readlines
puts " loc, tloc, randoop.pit"
projects.each do |p|
  p.chomp!
  next if p =~ /^#/
  res = read_pit(p, lastf(p,"randoop","build")).to_i
  case ARGV[0]
  when /-nz/
    next if res < 1
  when /-z/
    next if res < 0
  else
  end
  puts "#{p}, #{loc[p]}, #{tloc[p]}, #{res}"
end
