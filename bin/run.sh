#!/bin/bash

export JAVA_HOME=/scratch/sw/jdk1.7.0_21
export PATH=/scratch/sw/jdk1.7.0_21/bin:$PATH
export ANDROID_HOME=/scratch/ext_src/android-sdk-linux
export PATH=$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools:$PATH

case x$tsttype in
  x)
  echo "tsttype:<original,randoop>";
  exit 1;;
esac

case x$score in
  x)
  echo "score:<null,emma,pit,cobertura>";
  exit 1;;
esac


runone() {
  fake checkout-$1
  echo $(date +'%a %r') $tsttype $1 $(grep $1 etc/loc.txt | cut -d\  -f1 ) | tee -a logs/log.txt
  fake clean-$1 >> logs/log.txt
  /usr/bin/timeout -s 9 $((3600*1))s stdbuf -oL fake $tsttype-$1 score=$score >> logs/log.txt
}

runall() {
  echo > logs/log.txt
  for i in $(./bin/cat etc/projects.txt);
  do
    runone $i;
  done
}

case x"$1" in
  x) runall;;
  *) runone $1;;
esac

