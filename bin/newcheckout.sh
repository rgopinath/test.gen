#!/bin/bash
case x$1 in
  x) exit 0;;
  *) (cd projects/$1 &&  git reset --hard && git clean -xfd && git pull --rebase );;
esac
