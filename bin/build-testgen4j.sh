mkdir -p $TEST_DIR
pkg=$2
echo "building $pkg"
TOOLS=/usr/lib/jvm/java-1.6.0-sun-1.6.0.39.x86_64/lib/tools.jar
java -cp $TOOLS:$root/lib/classdoc.jar:project.jar:$root/lib/testgen4j.jar:$root/lib/xerces.jar:$CLASSPATH \
  classdoc -doclet com.spikesource.spiketestgen.SpikeTestGen -d $TEST_DIR/ -docpath project.jar
echo "compiling $pkg"

for i in $TEST_DIR/*.java; do
  echo processing $i : $pkg;
  perl -pi -e 'print "package '$pkg'.testgen4j;\n" if $. == 1' $i;
  echo
done

javac -d target/test-classes -classpath $TOOLS:$root/lib/junit-3.jar:project.jar:$root/lib/xmlutil.jar:$root/lib/xtype.jar:$root/lib/jtestcase.jar:$CLASSPATH $TEST_DIR/*.java
