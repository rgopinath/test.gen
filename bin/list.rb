#!/usr/bin/ruby

#Name, (Original Emma,Original Mutation) (Randoop Emma, Randoop Mutation)

def lastf(p, ttype, dir)
  lst = Dir.glob("#{dir}/#{ttype}.#{p}.*").sort_by{|f| File.ctime(f)}
  lst[0]
end

def read_pit(p, fname)
  return -1 if fname.nil?
  begin
    s = File.read(fname).chomp.split(/:/)
    s[-1].chomp
  rescue
    -1
  end
end
def read_emma(p, fname)
  return -1 if fname.nil?
  begin
    s = File.readlines(fname).grep(/all classes/)[0].split(/\s+/)
    s[6]
  rescue
    -1
  end
end

loc = {}
tloc = {}
def show_cloc(p)
  #x = %x[./bin/cloc.pl --quiet --csv --match-f='java$' projects/#{p}].split(/\n/)
  #x[2].split(/,/)[-1]
  
end

File.readlines('db/all.java_loc.list').each do |x|
  x.chomp!
  y = x.split(/ +/)
  loc[y[1]] = y[0].to_i
end


File.readlines('db/all.test_loc.list').each do |x|
  x.chomp!
  y = x.split(/ +/)
  tloc[y[1]] = y[0].to_i
end



projects = File.open('etc/projects.txt').readlines
puts "project, loc, tloc, original.pit"
projects.each do |p|
  p.chomp!
  #STDERR.puts p.inspect
  next if p =~ /^#/
  original_pit = read_pit(p, lastf(p,"original","build")).to_i
  case ARGV[0]
  when /-nz/
    next if original_pit < 1
  when /-z/
    next if original_pit < 0
  else
  end
  #original_emma = read_emma(p, lastf(p,"original","build/emma"))
  #next if original_emma == -1
  #randoop_pit = read_pit(p, lastf(p,"randoop","build"))
  #next if randoop_pit == -1
  #randoop_emma = read_emma(p, lastf(p,"tpalus","build/emma"))
  #next if randoop_emma == -1
  #puts "#{p}[#{show_cloc(p)}] (#{original_pit}, #{original_emma}) (#{randoop_pit}, #{randoop_emma})"
  puts "#{p}, #{loc[p]}, #{tloc[p]}, #{original_pit}"
end
