mkdir -p $TEST_DIR
TOOLS=/usr/lib/jvm/java-1.6.0-sun-1.6.0.39.x86_64/lib/tools.jar

java -cp $TOOLS:$root/lib/junit-3.jar:project.jar:$root/lib/xmlutil.jar:$root/lib/xtype.jar:$root/lib/xerces.jar:$root/lib/jtestcase.jar:target/test-classes:$CLASSPATH -Xmx1024m junit.textui.TestRunner ${TID}PackageTestSuite
