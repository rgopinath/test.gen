$(if $(filter $(MAKE_VERSION), 3.82),, $(error Use fake current $(MAKE_VERSION)))

.PHONY: checkout all clobber clean $(checkout.all)
.SECONDARY: $(addprefix projects/,$(names))

root:=$(CURDIR)

types=$(shell $(root)/bin/cat $(root)/etc/types.txt)
names=$(shell $(root)/bin/cat $(root)/etc/projects.txt)
date=$(shell date +%F,%R)
maxtimeout=3600

all:
	@echo use 'make <type>-<project>'
	@echo projects = $(names)
	@echo types = $(types)

runtests: $(types)
	@echo $@ done.

checkout: $(addprefix checkout-,$(names))
	@echo $@ done.

checkout-%: | projects/%/.checkedout
	@echo $@ done.

projects build tmp .gen home logs logs/files build/emma build/mockit build/pit build/cobertura build/codecover build/files : ;  mkdir -p $@

projects/%/.checkedout: | projects build tmp .gen build/emma build/pit build/cobertura
	cd projects && ../bin/checkout $*
	mkdir -p projects/$*/src/main projects/$*/src/test
	$(root)/bin/cloc.pl projects/$*/src/main > $(root)/logs/$*.main.cloc
	$(root)/bin/cloc.pl projects/$*/src/test > $(root)/logs/$*.original.test.cloc
	cd projects/$* && tar -cf $(root)/build/files/original.$*.tar src/test
	cat projects/$*/pom.xml > projects/$*/.pom.xml
	$(root)/bin/updatepom.rb -$(score) projects/$*/.pom.xml > projects/$*/pom.xml
	cp etc/Makefile.tmpl projects/$*/Makefile
	touch projects/$*/.checkedout

clean: $(addprefix clean-,$(names)) logs
	truncate -s 0 logs/log.txt

clean-%: | projects/%/.git
	cd projects/$* && chmod -R 755 . && git clean -xfd && git reset --hard && ( git stash && git stash drop ; git pull --rebase )
	cp etc/Makefile.tmpl projects/$*/Makefile
	cat projects/$*/pom.xml > projects/$*/.pom.xml
	$(root)/bin/updatepom.rb -$(score) projects/$*/.pom.xml > projects/$*/pom.xml
	touch projects/$*/.checkedout

clobber-%:
	cd projects && rm -rf $*

clobber:
	echo > logs/log.txt
	rm -rf logs/*.log
	rm -rf logs/*.cloc
	rm -rf logs/files; mkdir logs/files
	rm -rf tmp home build/emma build/pit build/mockit build/codecover build/cobertura

dirs: tmp home logs/files projects build/emma build/pit build/mockit build/codecover build/cobertura build/files

#-----------------------------------------------------------------

define namegen =
all-$1 : $(addsuffix -$(1),$(types))
	@echo $$(@) done.
endef

define testgen =
$1-all : $(addprefix $(1)-,$(names))
	@echo $$(@) done.

$1-% : projects/%/.$(1).done
	@echo $$(@) done.

%/.$1.done : | %/.git dirs
	$$(root)/bin/cleantmp
	@echo "`date +'%r'` to `date --date '1 hour' +'%r'`"
	- env timeout=$$(maxtimeout) root=$$(root) \
		$$(MAKE) -C $$(*) date=$$(date) project=$$(shell basename $$(*)) root=$$(root) score=$$(score) type=$(1) runtests 2>&1 | \
		sed -e '/^[ ]*$$$$/d' -e "s#^#$$(*F)-$(1): #g" | \
			tr -dc '[:print:][:space:]' | \
			tee logs/$(1).$$(shell basename $$(*)).$$(date).log; echo $$$$SECONDS \
		> $$(root)/build/.$(1).$$(shell basename $$(*)).$$(date).time
	@echo ended at `date +'%r'`
	@find $$(*) > logs/files/$(1).$$(shell basename $$(*)).$$(date)
	@echo =============================================
endef

$(foreach var,$(types),$(eval $(call testgen,$(var))))
$(foreach var,$(names),$(eval $(call namegen,$(var))))

#-----------------------------------------------------------------

